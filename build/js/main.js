var buttonModel = document.querySelector('.js__button-model');
var buttonParam = document.querySelector('.js__button-parameters');
var model = document.querySelector('.lk__model');
var parameters = document.querySelector('.lk__parameters');

buttonModel.addEventListener('click', function(evt) {
    evt.preventDefault();

      $(model).fadeOut();
      $(parameters).fadeIn();

});

buttonParam.addEventListener('click', function(evt) {
    evt.preventDefault();

      $(parameters).fadeOut();
      $(model).fadeIn();

});

document.querySelectorAll(".regist__input--file").addEventListener("change", function () {
  if (this.files[0]) {
  var fr = new FileReader();

    fr.addEventListener("load", function () {
      document.querySelector(".regist__input-file-indicator").style.backgroundImage = "url(" + fr.result + ")";
      }, false);

  fr.readAsDataURL(this.files[0]);
}
});
