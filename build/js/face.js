// var DEV_PATH = true;
var DEV_PATH = false;
var CNS = false;

// var pathJSON = ["Path",{
//     "applyMatrix":true,
//     "selected":true,
//     "segments":[[[232.34862,119.81312],[0.89286,-20.64145],[-0.89286,20.64145]],[[217.43491,195.18119],[12.69247,-29.95626],[-12.69247,29.95626]],[[157.06684,261.23813],[23.61902,-0.95853],[-23.61902,0.95853]],[[96.75092,199.12904],[13.51542,29.84254],[-13.51542,-29.84254]],[[79.71666,120.81312],[-0.33054,22.01339],[0.33054,-22.01339]],[[97.76422,73.5105],[-13.20658,7.72245],[13.20658,-7.72245]],[[157.06684,68.44714],[-24.19334,-0.53722],[24.19334,0.53722]],[[220.74715,75.13281],[-13.00299,-7.1959],[13.00299,7.1959]]],
//     "closed":true,
//     "fillColor":[0.5,0.5,0.5,0.5],
//     "strokeColor":[0.46667,0.46667,0.46667],"strokeWidth":3
// }];
var pathJSON = {
    "applyMatrix":true,
    // "selected":true,
    // "segments":[[[232.34862,119.81312],[0.89286,-20.64145],[-0.89286,20.64145]],[[217.43491,195.18119],[12.69247,-29.95626],[-12.69247,29.95626]],[[157.06684,261.23813],[23.61902,-0.95853],[-23.61902,0.95853]],[[96.75092,199.12904],[13.51542,29.84254],[-13.51542,-29.84254]],[[79.71666,120.81312],[-0.33054,22.01339],[0.33054,-22.01339]],[[97.76422,73.5105],[-13.20658,7.72245],[13.20658,-7.72245]],[[157.06684,68.44714],[-24.19334,-0.53722],[24.19334,0.53722]],[[220.74715,75.13281],[-13.00299,-7.1959],[13.00299,7.1959]]], // 300
    "segments": [
        [[454.34862,313.81312],[7.79916,-47.27563],[-7.79916,47.27563]],
        [[419.43491,419.18119],[22.67102,-40.68233],[-22.67102,40.68233]],
        [[313.06684,525.23813],[42.79852,-1.42006],[-42.79852,1.42006]],
        [[200.75092,416.12904],[24.81887,49.41474],[-24.81887,-49.41474]],
        [[165.71666,280.81312],[5.27618,48.18613],[-5.27618,-48.18613]],
        [[179.76422,148.5105],[-24.93687,25.45929],[24.93687,-25.45929]],
        [[316.06684,130.44714],[-55.87887,0.34269],[55.87887,-0.34269]],
        [[450.74715,155.13281],[-22.53058,-33.45237],[22.53058,33.45237]]
    ], // 600
    "closed":true,
    "fillColor":[0.5,0.5,0.5,0.5],
    "strokeColor":[0.46667,0.46667,0.46667],
    "strokeWidth":3
};

var maskShapes = {
    circle: [
        [[461.34862,296.81312],[3.07164,-43.92709],[-3.07164,43.92709]],
        [[424.43491,422.18119],[25.37578,-32.148],[-25.37578,32.148]],
        [[313.06684,470.23813],[43.70701,-0.90593],[-43.70701,0.90593]],
        [[196.75092,423.12904],[27.48015,34.82387],[-27.48015,-34.82387]],
        [[159.71666,285.81312],[-0.27745,46.03545],[0.27745,-46.03545]],
        [[197.76422,177.5105],[-27.38364,26.65289],[27.38364,-26.65289]],
        [[316.06684,134.44714],[-46.53816,-1.28101],[46.53816,1.28101]],
        [[436.74715,185.13281],[-25.44664,-29.15117],[25.44664,29.15117]]
    ],
    oval: [
        [[457.34862,297.81312],[6.15253,-57.33984],[-6.15253,57.33984]],
        [[417.43491,458.18119],[23.90897,-42.50455],[-23.90897,42.50455]],
        [[314.06684,524.23813],[41.49337,0.93304],[-41.49337,-0.93304]],
        [[201.75092,450.12904],[25.80152,46.82456],[-25.80152,-46.82456]],
        [[169.71666,279.81312],[-0.34928,56.19373],[0.34928,-56.19373]],
        [[201.76422,144.5105],[-24.41769,34.01906],[24.41769,-34.01906]],
        [[315.06684,87.44714],[-47.33014,0.09601],[47.33014,-0.09601]],
        [[440.74715,148.13281],[-25.24467,-38.02543],[25.24467,38.02543]]
    ],
    square: [
        [[452.34862,306.81312],[7.11931,-61.41198],[-7.11931,61.41198]],
        [[417.43491,458.18119],[20.56903,-28.11162],[-20.56903,28.11162]],
        [[321.06684,480.23813],[41.88634,0.43345],[-41.88634,-0.43345]],
        [[201.75092,450.12904],[27.56959,34.42997],[-27.56959,-34.42997]],
        [[169.71666,279.81312],[-0.81451,62.27169],[0.81451,-62.27169]],
        [[201.76422,144.5105],[-24.32486,22.1018],[24.32486,-22.1018]],
        [[316.06684,130.44714],[-48.23623,-1.3129],[48.23623,1.3129]],
        [[442.74715,156.13281],[-23.71314,-28.47252],[23.71314,28.47252]]
    ],
    triangle: [
        [[454.34862,313.81312],[7.79916,-47.27563],[-7.79916,47.27563]],
        [[419.43491,419.18119],[22.67102,-40.68233],[-22.67102,40.68233]],
        [[313.06684,525.23813],[42.79852,-1.42006],[-42.79852,1.42006]],
        [[200.75092,416.12904],[24.81887,49.41474],[-24.81887,-49.41474]],
        [[165.71666,280.81312],[5.27618,48.18613],[-5.27618,-48.18613]],
        [[179.76422,148.5105],[-24.93687,25.45929],[24.93687,-25.45929]],
        [[316.06684,130.44714],[-55.87887,0.34269],[55.87887,-0.34269]],
        [[450.74715,155.13281],[-22.53058,-33.45237],[22.53058,33.45237]]
    ]
}

var hitOptions = {
    segments: true,
    stroke: true,
    fill: true,
    tolerance: 5
};

// $(function(){
var pathObject;

if(DEV_PATH){
    var values = {
        points: 8,
        radius: 100
    };
    
    createPaths();
    function createPaths() {
        pathObject = createBlob(view.size * Point.random(), values.radius, values.points);
        
        pathObject.fillColor = new Color(0.5, 0.5, 0.5, 0.5);
        pathObject.strokeColor = '#777';
        pathObject.strokeWidth = 3;

    }
    function createBlob(center, maxRadius, points) {
        var path = new Path();
        path.closed = true;
        for (var i = 0; i < points; i++) {
            var delta = new Point({
                length: (maxRadius * 0.5) + (Math.random() * maxRadius * 0.5),
                angle: (360 / points) * i
            });
            path.add(center + delta);
        }
        path.smooth();
        return path;
    }
}else{
    pathObject = new Path(pathJSON);
    // path.importJSON(pathJSON);
    // path.smooth();
    // function onResize(event) {
    //     // Whenever the window is resized, recenter the path:
    //     path.position = view.center;
    // }
}

// pathObject.selected = false;
console.log(pathObject);

    var segment, path;
    var movePath = false;
    function onMouseDown(event) {
        segment = path = null;
        var hitResult = project.hitTest(event.point, hitOptions);
        if (!hitResult)
            return;
    
        if (CNS && event.modifiers.shift) {
            if (hitResult.type == 'segment') {
                hitResult.segment.remove();
            };
            return;
        }
    
        if (hitResult) {
            path = hitResult.item;
            if (hitResult.type == 'segment') {
                segment = hitResult.segment;
            } else if (CNS && hitResult.type == 'stroke') {
                var location = hitResult.location;
                segment = path.insert(location.index + 1, event.point);
                path.smooth();
            }
        }
        movePath = hitResult.type == 'fill';
        if (movePath)
            project.activeLayer.addChild(hitResult.item);
    } 
    
    function onMouseMove(event) {
        project.activeLayer.selected = false;
        if (event.item)
            event.item.selected = true;
        // console.log(project.activeLayer.selected, event.item.selected);
        
    }
    
    function onMouseDrag(event) {
        if (segment) {
            segment.point += event.delta;
            path.smooth();
        } else if (path) {
            path.position += event.delta;
        }
    }
    function onMouseUp(event){
        // if(DEV_PATH && event.item)
        if(event.item){
            var json = JSON.parse(event.item.exportJSON());            
            console.log(JSON.stringify(json[1].segments));
        }
    }
// });



$(function(){

    window.getFacePath = function (cb) {
        var canvas = document.getElementById('face_canvas');
        pathObject.selected = false;
        setTimeout(function(){
            var path = canvas.toDataURL();
            cb(path);
            // path = cropImage(path, function (data) {
            //     if(cb)
            //         cb(path);
            // });
        }, 1);
    }
    
    window.setFaceMaskShape = function(shape) {
        if(maskShapes[shape]){
            var center = pathObject.position;
            pathObject.segments = maskShapes[shape];
            pathObject.position = center;
        }
    }

    window.setFaceMaskCenter = function(center){
        pathObject.position = new Point(center.x, center.y);
    }
    
    window.cancelFaceMaskSelection = function() {
        pathObject.selected = false;
    }
});