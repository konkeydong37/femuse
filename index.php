<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title></title>
        <script src="./js/bundle.js"></script>
        <script src="./js/app.js"></script>
        <style>
            html, body {
                margin: 0;
                border: 0;
                padding: 0;
            }
            .canvas-container {
                background-color: #f3f3f3;
                position: fixed !important;
                margin: 0;
                border: 0;
                padding: 0;
            }
            #ui {
                position: fixed;
                top: 0px;
            }
            #ui .active {
                border: 1px solid black;
            }
            .photo_wrap, .body_wrap {
                width: 386px;
                /* width: 600px; */
                /* width: 50%; */
                margin: 50px auto;
                position: relative;
            }
            #face_img, #body_img {
                width: 100%;
            }
            #face_canvas, #body_canvas {
                position: absolute;
                top: 0;
                bottom: 0;
                width: inherit;
                height: 100%;
            }
            #mask_forms {
                float: left;
                margin-top: 50px;
            }
            #mask_forms img {
                margin: auto;
                display: block;
            }
            .mask_form.active {
                border: 1px solid black;
            }
            #body_mask_forms, #head_mask_forms {
                position: fixed;
                top: 50px;
            }
            #body_mask_forms {
                right: 0;
            }
        </style>
    </head>
    <body>
        <div class="photo_wrap">
            <img id="face_img" src="./img/396599_76_i_024.png">
            <canvas id="face_canvas" resize data-paper-resize="true" />
        </div>
        <div class="body_wrap">
            <img id="body_img" src="./img/396599_76_i_024.png">
            <canvas id="body_canvas" resize data-paper-resize="true" />
        </div>
        <div id="ui">
            <button id="send">SEND</button>
            <label>лицо: <input type="file" name="face_file" id="face_file"></label>
            <label>тело: <input type="file" name="body_file" id="body_file"></label>
            <!--
                <input type="file" name="photo_file" id="photo_file"> 
                <label>лицо/тело<input type="checkbox" checked id='is_face' /></label> 
            -->
            <div id="head_mask_forms" class="editor__face">
                <div class="mask_form" data-value="circle">
                    <img src='./img/circle.png' alt='circle' />
                </div>
                <div class="mask_form" data-value="oval">
                    <img src='./img/oval.png' alt='oval' />
                </div>
                <div class="mask_form" data-value="square">
                    <img src='./img/square.png' alt='square' />
                </div>
                <div class="mask_form active" data-value="triangle">
                    <img src='./img/triangle.png' alt='triangle' />
                </div>
            </div>
            <div id="body_mask_forms" class="editor__body">
                <div class="mask_form" data-value="circle">
                    <img src='./img/circle.png' alt='circle' />
                </div>
                <div class="mask_form" data-value="oval">
                    <img src='./img/oval.png' alt='oval' />
                </div>
                <div class="mask_form" data-value="square">
                    <img src='./img/square.png' alt='square' />
                </div>
                <div class="mask_form active" data-value="triangle">
                    <img src='./img/triangle.png' alt='triangle' />
                </div>
            </div>
        </div>
        <img id="path" style="display: none;" src="">
        <script src="./js/face.js" type="text/paperscript" canvas="face_canvas"></script>
        <script src="./js/body.js" type="text/paperscript" canvas="body_canvas"></script>
    </body>
</html>